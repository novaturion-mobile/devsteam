import 'package:devsteam_test/app/photo.dart';
import 'package:flutter/material.dart';

import 'fullscreen_photo_widget.dart';

class PhotosListItemWidget extends StatelessWidget {
  final Photo photo;

  PhotosListItemWidget({Key key, this.photo}) : super(key: key);

  void viewDismissiblePhotoWidget(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (context) => FullscreenPhotoWidget(
            key: ValueKey(photo.id),
            imageWidget: Image.network(photo.regularUrl)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GridTile(
      child: GestureDetector(
        onTap: () => viewDismissiblePhotoWidget(context),
        child: Image.network(photo.smallUrl, fit: BoxFit.cover),
      ),
      footer: GestureDetector(
        onTap: () => viewDismissiblePhotoWidget(context),
        child: GridTileBar(
          backgroundColor: Colors.black45,
          title: Text(photo.name != null ? photo.name : ""),
          subtitle: Text(photo.author != null ? photo.author : ""),
        ),
      ),
    );
  }
}
