import 'package:flutter/material.dart';

class FullscreenPhotoWidget extends StatelessWidget {
  final Image imageWidget;

  const FullscreenPhotoWidget({Key key, this.imageWidget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Container(
        child: imageWidget,
      ),
    );
  }
}
