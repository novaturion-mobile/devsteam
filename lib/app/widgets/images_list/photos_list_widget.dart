import 'package:devsteam_test/app/photo.dart';
import 'package:flutter/material.dart';

import 'photos_list_item_widget.dart';

class PhotosListWidget extends StatelessWidget {
  final List<Photo> photos;

  PhotosListWidget({Key key, this.photos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.extent(
      maxCrossAxisExtent: 250,
      padding: const EdgeInsets.all(4),
      mainAxisSpacing: 4,
      crossAxisSpacing: 4,
      children: List.generate(
        photos.length,
        (index) => PhotosListItemWidget(photo: photos[index]),
      ),
    );
  }
}
