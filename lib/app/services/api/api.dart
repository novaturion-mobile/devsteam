import 'package:http/http.dart' as http;
import 'api_service.dart';

export 'api_service.dart';
export 'api_keys.dart';

class API {
  Future<http.Response> fetchLatestPhotos(http.Client client, int page) async {
    return await client.get(APIService.getLatestPhotosURL());
  }
}
