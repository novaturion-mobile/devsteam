class APIKeys {
  static String get latestPhotosURL => "https://api.unsplash.com/photos/";
  static String get token =>
      "cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0";
  // static String get _token1 =>
  //     "ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9";
  // static String get _token2 =>
  //     "896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043";
  static Map<String, String> get parameters => {"client_id": token};
}
