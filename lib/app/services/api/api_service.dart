import 'api_keys.dart';

class APIService {
  static String getLatestPhotosURL() {
    String urlParameters = "";
    APIKeys.parameters.forEach((key, value) {
      urlParameters += "?" + key + "=" + value;
    });
    return (APIKeys.latestPhotosURL.endsWith("/") ? APIKeys.latestPhotosURL : APIKeys.latestPhotosURL + "/") +
        urlParameters;
  }
}
