import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:devsteam_test/app/photo.dart';

List<Photo> parsePhotos(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed
      .map<Photo>(
        (json) => Photo(
          json['id'],
          rawUrl: json['urls']['raw'],
          fullUrl: json['urls']['full'],
          regularUrl: json['urls']['regular'],
          smallUrl: json['urls']['small'],
          thumbnailUrl: json['urls']['thumb'],
          name: json['description'],
          author: json['user']['username'],
        ),
      )
      .toList();
}

Future<List<Photo>> parsePhotosIsolate(String responseBody) async {
  return compute(parsePhotos, responseBody);
}
