import 'package:devsteam_test/app/photo.dart';
import 'package:devsteam_test/app/services/api/api.dart';
import 'package:devsteam_test/app/services/parsers/json_parser.dart'
    as JSONParser;
import 'package:http/http.dart' as http;

Future<List<Photo>> getPhotosList({int page = 0}) {
  final response = API().fetchLatestPhotos(http.Client(), page);
  return response.then((value) => JSONParser.parsePhotosIsolate(value.body));
}
