class Photo {
  final String id;
  final String name;
  final String author;
  final String rawUrl;
  final String fullUrl;
  final String regularUrl;
  final String smallUrl;
  final String thumbnailUrl;

  Photo(this.id, {this.rawUrl, this.fullUrl, this.regularUrl, this.smallUrl, this.thumbnailUrl, this.name, this.author});
}
