import 'package:devsteam_test/app/widgets/images_list/photos_list_widget.dart';
import 'package:devsteam_test/app/photo.dart';
import 'package:devsteam_test/app/services/parsers/widgets_parser.dart'
    as WidgetsParser;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final String title;

  HomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: FutureBuilder<List<Photo>>(
        future: WidgetsParser.getPhotosList(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? PhotosListWidget(photos: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
