import 'package:devsteam_test/app/pages/home_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Devsteam Test';
    final ThemeData themeData = ThemeData.dark();

    return MaterialApp(
      theme: themeData,
      title: appTitle,
      home: HomePage(title: appTitle),
    );
  }
}
